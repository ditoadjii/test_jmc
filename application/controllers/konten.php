<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konten extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		$this->load->model('konten_model');
	}


		public function index()
	{

			$data['main_view'] = 'konten';
			$data['provinsi']=$this->konten_model->get_provinsi();
			$this->load->view('template', $data);
	
	}

		public function tambah()
	{
		if($this->konten_model->tambah()) {
					$this->session->set_flashdata('notif', 'Tambah Data berhasil');
					redirect('konten/index');
				} else {
					$this->session->set_flashdata('notif', 'Tambah Data gagal');
					redirect('konten/index');

					}
	}

		public function ubah()
	{

			$this->form_validation->set_rules('nama_provinsi', 'nama_provinsi', 'trim|required');
			

			if ($this->form_validation->run() == TRUE) {
				if($this->konten_model->ubah() == TRUE){
					$this->session->set_flashdata('notif', 'Ubah provinsi berhasil');
					redirect('konten/index');
				} else {
					$this->session->set_flashdata('notif', 'Ubah provinsi gagal');
					redirect('konten/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('konten/index');
			}

	}

		public function hapus()
	{

			if($this->konten_model->hapus() == TRUE){
				$this->session->set_flashdata('notif', 'Hapus Provinsi Berhasil');
				redirect('konten/index');
			} else {
				$this->session->set_flashdata('notif', 'Hapus Provinsi gagal');
				redirect('konten/index');
			}

		
	}

		public function get_provinsi_by_id($id)
	{
		

			$data = $this->konten_model->get_provinsi_by_id($id);
			echo json_encode($data);

		
	}

}

/* End of file konten.php */
/* Location: ./application/controllers/konten.php */