<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		$this->load->model('kabupaten_model','kabupaten');
	}

		public function index()
	{


			$data['main_view'] = 'kabupaten';
			$data['data_provinsi']=$this->kabupaten->get_data_provinsi();			
			$this->load->view('template', $data);


	
	}
	
		public function tambah()
	{
		if($this->kabupaten->tambah()) {
					$this->session->set_flashdata('notif', 'Tambah Data berhasil');
					redirect('kabupaten/index');
				} else {
					$this->session->set_flashdata('notif', 'Tambah Data gagal');
					redirect('kabupaten/index');

					}
	}

		public function ubah()
	{

			$this->form_validation->set_rules('nama_provinsi', 'nama_provinsi', 'trim|required');
			$this->form_validation->set_rules('nama_kabupaten', 'nama_kabupaten', 'trim|required');
			$this->form_validation->set_rules('jumlah_penduduk', 'jumlah_penduduk', 'trim|required');
			

			if ($this->form_validation->run() == TRUE) {
				if($this->kabupaten_model->ubah() == TRUE){
					$this->session->set_flashdata('notif', 'Ubah provinsi berhasil');
					redirect('konten/index');
				} else {
					$this->session->set_flashdata('notif', 'Ubah provinsi gagal');
					redirect('konten/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('konten/index');
			}

	}	

		public function getKabupatenByIdProvinsi($id_provinsi)
	{
		$kabupaten = $this->kabupaten->getKabupatenById($id_provinsi);
		foreach($kabupaten as $data){
			echo "
			<tr>
				<td>1</td>
				<td>$data->nama_provinsi</td>
				<td>$data->nama_kabupaten</td>
				<td>$data->jumlah_penduduk</td>
			</tr>
			";
		}
	}

		
	

}

/* End of file kabupaten.php */
/* Location: ./application/controllers/kabupaten.php */