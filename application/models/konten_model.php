<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konten_model extends CI_Model {

		public function get_provinsi(){

		return $this->db->get('provinsi')
						->result();
	}

		public function get_provinsi_by_id($id)
	{
		return $this->db->where('id_provinsi', $id)
						->get('provinsi')
						->row();
	}		

		public function tambah()
	{
		$data = array(

				'nama_provinsi'		=> $this->input->post('nama_provinsi'),
			);

		$this->db->insert('provinsi', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

		public function ubah()
	{
		$data = array(
			
				'nama_provinsi' 	=> $this->input->post('nama_provinsi')
			);

		$this->db->where('id_provinsi', $this->input->post('id_provinsi'))
				 ->update('provinsi', $data);
		
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

		public function hapus()
	{
		
		$this->db->where('id_provinsi', $this->input->post('hapus_id_provinsi'))
				 ->delete('provinsi');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	

}

/* End of file konten_model.php */
/* Location: ./application/models/konten_model.php */