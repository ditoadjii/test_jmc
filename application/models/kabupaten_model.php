<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends CI_Model {

		public function get_data_provinsi()
	{
		return $this->db->get('provinsi')
						->result();
	}


		public function tambah()
	{
		$data = array(

				'nama_kabupaten'		=> $this->input->post('nama_kabupaten'),
				'jumlah_penduduk'		=> $this->input->post('jumlah_penduduk'),
				'id_provinsi'		=> $this->input->post('id_provinsi')

			);

		$this->db->insert('kabupaten', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function getKabupatenById($id_provinsi){
		return $this->db->join('provinsi','provinsi.id_provinsi=kabupaten.id_provinsi')
		                ->where('kabupaten.id_provinsi', $id_provinsi)
						->get('kabupaten')
						->result();
	}

	

}

/* End of file kabupaten_model.php */
/* Location: ./application/models/kabupaten_model.php */