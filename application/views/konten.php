        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Inputan</strong>
                </div>

                <div class="card-body card-block">
                    <form action="<?php echo base_url('index.php/konten/tambah'); ?>" method="post">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="email-input" class=" form-control-label" style="">Provinsi</label>    
                                <input required type="text" name="nama_provinsi" id="tag" class="form-control" style="width: 350px">
                            </div>

                            
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm" name="submit" style="margin-left: 400px;">
                              <i class="fa fa-dot-circle-o"></i>Submit
                          </button>
                      </div>
                  </form>

              </div>

          </div>

        </div>

        <div class="col-lg-7">
              <?php if ($this->session->flashdata('notif')): ?>
                  <div class="alert alert-info"><?= $this->session->flashdata('notif');?></div>
              <?php endif ?>

          <div class="card">
                <div class="card-header">
                    <strong class="card-title">List Provinsi</strong>
                </div>
                <div class="card-body">
                    <table id="bootstrap-data-table" class="table table-bordered table-dark">

                        <thead>
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">Nama Provinsi</th>
                              <th scope="col">Action</th>
                          </tr>
                        </thead>

                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($provinsi as $p) {
                              echo '
                              <tr>
                              <td>'.$no.'</td>
                              <td>'.$p->nama_provinsi.'</td>
                              <td>
                              <a href="#edit" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_ubah_provinsi" onclick="prepare_ubah_provinsi('.$p->id_provinsi.')">Ubah</a>
                              <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_hapus_provinsi" onclick="prepare_hapus_provinsi('.$p->id_provinsi.')">Hapus</a>
                              </td>

                              </tr>
                              ';
                              $no++;
                          }
                              ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="modal_ubah_provinsi" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ubah Provinsi</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="<?php echo base_url('index.php/konten/ubah'); ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <input type="hidden" name="id_provinsi"  id="edt_id_provinsi">
                            <input type="text" class="form-control" placeholder="Nama Provinsi" name="nama_provinsi"  id="edt_nama_provinsi">

                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="modal_hapus_provinsi" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Konfirmasi Hapus Provinsi</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                        </div>
                    <form action="<?php echo base_url('index.php/konten/hapus'); ?>" method="post">
                        <div class="modal-body">
                            <input type="hidden" name="hapus_id_provinsi"  id="hapus_id_provinsi">
                            <p>Data akan terhapus !<b><span id="hapus_nama_provinsi"></span></b> </p>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-danger" name="submit" value="YA">
                            <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="<?=base_url();?>assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script type="text/javascript">

          function prepare_ubah_provinsi(id)
          {
              $("#id_provinsi").val(id);
              $.ajax({
                  type:"get",
                  url:"<?=base_url()?>index.php/konten/get_provinsi_by_id/"+id,
                  dataType:"json",
                  success:function (detail) {
                    $("#edt_id_provinsi").val(detail.id_provinsi);
                    $("#edt_nama_provinsi").val(detail.nama_provinsi);
                    
                }
            });

          }
          function prepare_hapus_provinsi(id)
          {
            $("#hapus_id_provinsi").val(id);
          
          }
        </script>

