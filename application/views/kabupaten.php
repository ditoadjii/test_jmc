        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Inputan</strong>
                </div>

                <div class="card-body card-block">
                    <form action="<?php echo base_url('index.php/kabupaten/tambah'); ?>" method="post">
                        <div class="row form-group">
                            <div class="col col-md-12">
                                <label for="email-input" class=" form-control-label" style="">Kabupaten</label>    
                                <input required type="text" name="nama_kabupaten" id="tag" class="form-control" style="width: 350px">
                            </div>
                            <div class="col col-md-12">
                                <label for="email-input" class=" form-control-label" style="">Jumlah Penduduk</label>    
                                <input required type="text" name="jumlah_penduduk" id="tag" class="form-control" style="width: 350px">
                            </div>
                            <div class="col col-md-12">
                                <label for="email-input" class=" form-control-label" style="">Provinsi</label>    
                                <select name="id_provinsi" class="form-control" style="width: 350px;">
                                    <option></option>
                                        <?php foreach ($data_provinsi as $provinsi): ?>
                                            <option value="<?=$provinsi->id_provinsi?>"><?=$provinsi->nama_provinsi?></option>
                                        <?php endforeach ?> 
                                
                              </select>
                            </div>   
                        </div>
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm" name="submit" style="margin-left: 400px;">
                          <i class="fa fa-dot-circle-o"></i>Submit
                        </button>
                      </div>
                    </form>
                    
                </div>
                
            </div>
            
        </div>
        <div class="col-lg-7">
              <?php if ($this->session->flashdata('notif')): ?>
                  <div class="alert alert-info"><?= $this->session->flashdata('notif');?></div>
              <?php endif ?>

          <div class="card">
                <div class="card-header">
                    <strong class="card-title">List Provinsi</strong>
                </div>
                <div class="card-body">
                    <select id="select-province">
                        <?php foreach($data_provinsi as $data) {?>
                            <option value="<?= $data->id_provinsi?>"><?= $data->nama_provinsi?></option>
                        <?php } ?>
                    </select>
                    <table id="table-kabupaten" class="table table-bordered table-dark">

                        <thead>
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">Nama Provinsi</th>
                              <th scope="col">Nama Kabupaten</th>
                              <th scope="col">Jumlah Penduduk</th>
                          </tr>
                        </thead>

                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

      
